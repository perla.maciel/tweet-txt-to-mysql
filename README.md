# Tweet en txt a base de datos en MySQL

Esta es una libreria creada con el fin de depurar los datos que se obtienen de la extracción de tweets, solo manteniendo lo mas importante.

## Importante 

* Para usar esta libreria se debe de contar con la instalación minima del JDK 7.

* Además de tener acceso a un servidor de base de datos MySQL donde recidirán nuestros tweets depurados.

## Como usar

La libreria tiene que ser modificada en el apartado de [MySqlConn](https://gitlab.com/perla.maciel/tweet-txt-to-mysql/blob/master/src/BD/MySqlConn.java)

### Example

```java
 connectionUrl =  "jdbc:mysql://localhost/twitter?user=root&password=password";
 ```

Esto con el fin de realizar la conexión a la base de datos deseada.

Posteoriormente compilamos nuevamente la libreria y ya podremos importarla.


## Métodos disponibles






