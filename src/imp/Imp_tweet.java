package imp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JFrame;


public class Imp_tweet {
    static BD.MySqlConn mysql = new BD.MySqlConn();
    ////////////////////////////////////////////////////////////////////////////
    private JFrame choose;
    private String directorio;
    
    public Imp_tweet() {
        try{            
            choose = new JFrame();
            choose.setVisible(false);
            choose.setLocationRelativeTo(null);
            JFileChooser directory = new JFileChooser("Select Directory");
            directory.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            directory.showOpenDialog(choose);       
            directorio = directory.getSelectedFile().toString();
        }catch(NullPointerException e){
            System.exit(0);
        }
        openDir();
        //Se cierra la ventana
        choose.dispose();
    }
    
    private void openDir(){
        File[] ficheros;
        File file = new File(directorio);
        //String format ;
        if (file.exists()){ // Directorio existe 
            ficheros = file.listFiles(); //Array de ficheros
            for (int x=0;x<ficheros.length;x++){
                try{           
                    openFile(ficheros[x].getPath());
              }catch(Exception e){}
            }
        }else { //Directorio no existe 
        }
    }
    
    private void openFile(String direcc){
      File archivo = null;
      FileReader fr = null;
      //BufferedReader br = null;
      try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File (direcc);
            if (archivo.exists() /*&& band == true*/){ // Directorio existe 
                if(archivo.isDirectory() == true){
                    File file = new File(direcc);
                    File[]files = file.listFiles();
                    for (int j = 0; j < files.length; j++) {
                        
                        leer(files[j].getPath());
                    }
                          
                }else{
                        leer(archivo.getPath());
                }    
              
            }else { //Directorio no existe 
            }  
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try{                    
                if( null != fr ){   
                   fr.close();     
                }                  
            }catch (Exception e2){ 
                e2.printStackTrace();
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public static void leer (String archivo) throws FileNotFoundException, ParseException {
        //String cadena = "";
        //int bandera=0,aux2=0;
        int i = 0;
        ArrayList <String> lista = new ArrayList();
        FileReader f = new FileReader(archivo);
        Scanner s = new Scanner (f);
        SimpleDateFormat parser = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy",Locale.ENGLISH);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        while(s.hasNext()) {
            //Quitamos los espacios en blancos no necesarios, las comas y comillas
            lista.add(s.nextLine().replaceAll(",","").replaceAll("\"", "").replaceAll("^\\s*","" ));
            //System.out.println(lista.get(aux2));
            //aux2++;
        }
        s.close();
        
        String fecha_creacion="";
        String id_tweet="";
        String text="";
        String language="";
        String retweet="";
        String geolacation="";
        String name="";
        String user_id="";
        
        for (i=0;i<lista.size();i++){
            if (lista.get(i).matches("^coordi.*")){
                //System.out.println(lista.get(i));
                while ((!lista.get(i).matches("^user:.*"))){
                    i++;
                    //System.out.println(lista.get(i));
                    if (lista.get(i).matches("^created_at.*")){
                        //Date date = parser.parse(aux);
                        Date date = parser.parse(lista.get(i).substring(12));
                        fecha_creacion = format.format(date);
                        System.out.println(fecha_creacion);
                    }
                    if (lista.get(i).matches("^id_str.*")){
                        id_tweet = lista.get(i).substring(8);
                        System.out.println(id_tweet);
                    }
                    if (lista.get(i).matches("^hasht.*")){
                        while((!lista.get(i).matches("^user_m.*"))){
                            i++;
                        }
                    }
                    
                    if (lista.get(i).matches("^tex.*")){
                        text = lista.get(i).substring(6);
                        System.out.println(text);   
                    }
                    if (lista.get(i).matches("^iso_lan.*")){
                        language = lista.get(i).substring(19);
                        System.out.println(language);   
                    }
                    if (lista.get(i).matches("^retweet_c.*")){
                        retweet = lista.get(i).substring(15);
                        System.out.println(retweet);   
                    }
                    if (lista.get(i).matches("^geo.*")){
                        geolacation = lista.get(i).substring(5);
                        System.out.println(geolacation);   
                    }
                }
                
                while ((!lista.get(i).matches("^entit.*"))){
                    i++;
                    if (lista.get(i).matches("^name.*")){
                        name = lista.get(i).substring(6);
                        System.out.println(name);   
                    }

                    if (lista.get(i).matches("^id_.*")){
                        user_id = lista.get(i).substring(8);
                        System.out.println(user_id+"\n\n\n\n\n");   
                    }
                }
                
                while ((!lista.get(i).matches("^in_reply_to_st.*"))){
                    i++;
                }
                
                guardar(id_tweet,text,fecha_creacion,language,retweet,geolacation,name,user_id);
                
            }//Fin de un tweet   
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    private static void guardar (String id_tweet, String text, String fecha_creacion, String language, String retweet, String geolacation, String name, String user_id){
        String query = "";
        query = "INSERT INTO TWEET VALUES ("+
                "'" + id_tweet + "'," +
                "'" + text + "'," +
                "'" + fecha_creacion + "'," +
                "'" + language + "'," +
                "'" + retweet + "'," +
                "'" + geolacation + "'," +
                "'" + name + "'," +
                "'" + user_id + "')";
        System.out.println(query);
        mysql.Update(query);
        
    } 
}
